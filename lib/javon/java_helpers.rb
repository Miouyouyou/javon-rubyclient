# -*- coding: utf-8 -*-
require 'set'

module Javon
  module JavaHelpers

    # Nested classes are currently omitted
    @@java_lang_map =
      Set[:AbstractMethodError, :Appendable, :ArithmeticException,
          :ArrayIndexOutOfBoundsException, :ArrayStoreException,
          :AssertionError, :AutoCloseable, :Boolean, :BootstrapMethodError,
          :Byte, :CharSequence, :Character, :'Character$Subset',
          :'Character$UnicodeBlock', :'Character$UnicodeScript', :Class,
          :ClassCastException, :ClassCircularityError, :ClassFormatError,
          :ClassLoader, :ClassNotFoundException, :ClassValue,
          :CloneNotSupportedException, :Cloneable, :Comparable, :Compiler,
          :Deprecated, :Double, :Enum, :EnumConstantNotPresentException, :Error,
          :Exception, :ExceptionInInitializerError, :Float, :IllegalAccessError,
          :IllegalAccessException, :IllegalArgumentException,
          :IllegalMonitorStateException, :IllegalStateException,
          :IllegalThreadStateException, :IncompatibleClassChangeError,
          :IndexOutOfBoundsException, :InheritableThreadLocal,
          :InstantiationError, :InstantiationException, :Integer,
          :InternalError, :InterruptedException, :Iterable, :LinkageError,
          :Long, :Math, :NegativeArraySizeException, :NoClassDefFoundError,
          :NoSuchFieldError, :NoSuchFieldException, :NoSuchMethodError,
          :NoSuchMethodException, :NullPointerException, :Number,
          :NumberFormatException, :Object, :OutOfMemoryError, :Override,
          :Package, :Process, :ProcessBuilder, :'ProcessBuilder$Redirect',
          :'ProcessBuilder$Redirect$Type', :Readable,
          :ReflectiveOperationException, :Runnable, :Runtime, :RuntimeException,
          :RuntimePermission, :SafeVarargs, :SecurityException,
          :SecurityManager, :Short, :StackOverflowError, :StackTraceElement,
          :StrictMath, :String, :StringBuffer, :StringBuilder,
          :StringIndexOutOfBoundsException, :SuppressWarnings, :System, :Thread,
          :'Thread$State', :'Thread$UncaughtExceptionHandler', :ThreadDeath,
          :ThreadGroup, :ThreadLocal, :Throwable, :TypeNotPresentException,
          :UnknownError, :UnsatisfiedLinkError, :UnsupportedClassVersionError,
          :UnsupportedOperationException, :VerifyError, :VirtualMachineError,
          :Void]

    @@primitives_types = Set[:void, :boolean, :byte, :short,
                             :char, :int, :long, :float, :double]

    @@numeric_types =
      Set[:byte, :short, :char, :int, :long, :float, :double,
          :'java.lang.Byte', :'java.lang.Short', :'java.lang.Character',
          :'java.lang.Integer', :'java.lang.Long', :'java.lang.Float',
          :'java.lang.Double', :'java.lang.Number']

    def self.is_primitive(type)
      return @@primitives_types.include?(type)
    end

    def self.is_numeric_type?(classname)
      return @@numeric_types.include?(classname.to_sym)
    end

    @@classname_symbols = /(\.\.\.|\[\])*/
    def self.full_class_name(classname)
      classname_only = classname.to_s.gsub(@@classname_symbols, '').to_sym
      if @@java_lang_map.include?(classname_only)
        return "java.lang.#{classname}".to_sym
      else
        return classname.to_sym
      end
    end
  end
end
