# -*- coding: utf-8 -*-
module Javon
  module JavaTypes
    # For integers, ranges exclude the last value.
    @@ranges =
      {:double => (-1.79769e+308..1.79769e+308),
       :long   => (-2**63..2**63-1),
       :int    => (-2**31..2**31-1)}.freeze

    def self.ranges
      return @@ranges
    end
  end
end


