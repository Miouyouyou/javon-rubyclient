# -*- coding: utf-8 -*-
module JavonClient

  def new(classname, *arguments)
    JavonClient::NewObject(classname.to_s, *arguments)
  end

end
