# -*- coding: utf-8 -*-
require 'javon/ruby_adapter'
require 'net/http'
require 'json'
require 'set'

module Javon
  module ObjectWrapper

    class MethodDelegator < BasicObject

      @@object_wrapper = ::Javon::ObjectWrapper
      @@instance_methods = ::Set[:'instance_of?', :'kind_of?', 
        :'eql?', :'javon_structure!']
      def initialize(base_structure)
        @base_structure = base_structure
      end
      def method_missing(method_name, *arguments, **signature_and_options)
        @@object_wrapper.invoke_method(@base_structure, method_name,
                                       *arguments, **signature_and_options)
      end

      # Methods containing special characters cannot be defined in Java.
      def kind_of?(classname)
        classname === self
      end
      alias :is_a? :kind_of?

      def instance_of?(classname)
        classname == ::Javon::ObjectWrapper::MethodDelegator
      end

      def eql?(object)
        self.equal?(object)
      end

      def javon_structure!
        @base_structure
      end

      def respond_to?(method_name)
        @@instance_methods.include?(method_name)
      end

    end

    def self.javon_save_option
      {save: @@save_id}
    end
      
    @@save_id = 0

    def self.new(classname, *arguments, signature: nil, **options)
      self.invoke_method(classname, :new, *arguments, signature: signature,
                         **options)
    end
    
    def self.invoke_method(receiver, method_name, *arguments, signature: nil,
                           **options)

      save_instruction = self.javon_save_option
      new_object_instruction = ::Javon::RubyAdapter.invoke_method(receiver,
        method_name,
        *arguments, 
        signature: signature).merge(save_instruction).merge(options)
      result = self.execute(new_object_instruction)
    
      if result['type'] == 'Reference'
        @@save_id += 1
        MethodDelegator.new(result)
      else
        ::Javon::RubyAdapter.javon_to_ruby(result)
      end
          
    end
    
    :private
    def self.execute(javon_instruction)
      ::Javon::JSONClient.send(javon_instruction)
    end
  end
  
  module JSONClient
    @@server_address = 'localhost'
    @@server_port    = '8080'
    @@http_client    = Net::HTTP.new(@@server_address, 
                                     @@server_port)
     
    def self.send(javon_structure)
      unless javon_structure.kind_of? Hash
        raise RuntimeError, "javon_structure needs to be some kind of Hash."
      end 
      
      http_answer = self.http_client.post('/', 
                                          javon_structure.to_json,
                                          {'Content-type' => 'application/json'}
                                          )
      
      begin
        return JSON.parse(http_answer_content(http_answer))
      rescue JSON::ParserError => error
        $stderr.puts("The JSON couldn't parse the answer.\n" << 
                     "Reason : #{error.message}")
        raise RuntimeError, "An error occured while parsing the answer as JSON."
      end
    end
    
    def self.server=(new_address: nil, new_port: nil)
      if new_address || new_port
        self.server_address=(new_address)
        self.server_port=(new_port)
        refresh_http_client
      end
    end
    
    :private
    def self.server_address
      @@server_address
    end
    def self.server_address=(new_address)
      @@server_address = new_address unless new_address.nil?
    end
    
    def self.server_port
      @@server_port
    end
    def self.server_port=(new_port)
      @@server_port = new_port unless new_port.nil?
    end
    
    def self.http_client
      @@http_client
    end
    def self.refresh_http_client
      @@http_client = Net::HTTP.new(self.server_address,
                                    self.server_port)
    end
    
    # Replace this method if you use another HTTP client.
    def self.http_answer_content(answer)
      answer.body
    end
    
  end
end
