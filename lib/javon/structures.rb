# -*- coding: utf-8 -*-
require 'javon/java_types'
require 'javon/java_helpers'

module Javon
  module Structures
    :public
    def self.null
      {category: :Object, type: :null}
    end

    def self.character(value)
      return null if value.nil?
      encoded_value = nil

      if value.is_a? Numeric
        return number(value, :char)
      else
        simple_character = value.to_s[0]
        if simple_character
          if can_be_displayed?(simple_character)
            encoded_value = simple_character
          else
            encoded_value = simple_character.codepoints[0]
          end
        else
          return null
        end
      end

      {category: :Object, type: :Character, value: encoded_value}
    end

    def self.string(value)
      return null if value.nil?
      {category: :Object, type: :String, value: value.to_s}
    end

    def self.class_object(name)
      return null if name.nil?
      {category: :Object, type: :Class, value: name.to_s}
    end

    def self.reference(id)
      {category: :Object, type: :Reference, value: id}
    end

    def self.boolean(value, classname = :boolean)
      if (value.nil? && !Javon::JavaHelpers.is_primitive(classname))
        return null
      end
      {category: :Boolean, value: (value ? true : false)}
    end

    def self.number(value, classname)
      return null if value.nil?

      if Javon::JavaHelpers.is_numeric_type?(classname)
        number_type = classname
      else
        number_type = :'java.lang.Number'
      end

      if !(value.kind_of? Numeric)
        converted_value = value.to_s[0]
      elsif !is_representable_number?(value)
        converted_value = convert_unrepresentable_number(value)
      else
        converted_value = value
      end

      return number_structure(converted_value, number_type)
    end

    def self.number_structure(value, classname)
      {category: :Number, type: classname.to_sym, value: value}
    end

    def self.array_object(array, classname, &extra_processing_block)
      {category: :Array, type: classname.to_sym,
       value: process_array_values(array, classname, &extra_processing_block)}
    end

    def self.invoke_method(object_structure, method_name, signature = nil,
                           arguments = nil)

      call_structure = {call: :invoke_method,
                        receiver: object_structure,
                        name: method_name}
      if signature
        call_structure.merge!({signature: signature, arguments: arguments})
      end

      return call_structure
    end

    def self.new_object(classname, signature = nil, arguments = nil)
      call_structure = {call: :new, receiver: classname.to_sym}

      if signature
        call_structure.merge!({signature: signature, arguments: arguments})
      end

      return call_structure
    end

    # Returns true if the character displayed as-is won't cause too much
    # trouble during debugging.
    # Control characters, unassigned characters and others 'special'
    # characters are a pain to debug...
    # @param provided_character  the character checked
    # @return true if the character can be displayed nicely
    def self.can_be_displayed?(character)
      /\p{C}/.match(character).nil?
    end

    def self.is_representable_number?(number)
      return Javon::JavaTypes.ranges[:double].cover?(number)
    end

    def self.convert_unrepresentable_number(num)
      # Why to_f ?
      # Remember that Javon is based on JSON. JSON numbers are, at worst,
      # double(-precision) numbers. BUT ! Infinity and NaN are not authorized
      # in JSON !
      # So Bignum values that would overflow to (-)Infinity when converted to
      # double on the server-side, must be represented as (-)'Infinity' in a
      # way that is legal in JSON.
      # The way used in Javon is to represent special floating-point numbers as
      # String. Example : Infinity becomes "Infinity".
      # Still, overflowing Bignum values must be converted to floating-point
      # numbers *before* converting them to String, in order to be represented
      # as "Infinity" or "-Infinity", else only the number will be converted to
      # String and an overflow will be still be triggered on the server-side,
      # which could provoke errors.
      return num.to_f.to_s
    end

    # TODO : La conversion des nombres non représentables nécessite de connaître
    #        la classe utilisée actuellement.
    def self.process_array_values(array, classname, &extra_processing)

      if block_given?
        post_processing = proc {|value, classname, extra_processing_block|
          extra_processing_block.yield(value, classname)
        }
      else
        post_processing = proc {|value| value}
      end

      array.map {|value|

        if value.kind_of?(Array)
          next(process_array_values(value, classname, &extra_processing))
        end

        preprocessed_value = value
        case value
        when Numeric
          if !is_representable_number?(value)
            preprocessed_value = self.number(value, classname)
          end
        end

        post_processing.call(preprocessed_value, classname, extra_processing)

      }

    end

  end
end
