# -*- coding: utf-8 -*-
require 'javon/structures'
require 'javon/java_helpers'

module Javon
  module RubyAdapter

    @@structures_map = {}
    { Javon::Structures.method(:null)    => [:null, :nil,
                                             :void, :'java.lang.Void'],
      Javon::Structures.method(:boolean) => [:boolean,
                                             :'java.lang.Boolean'],
      Javon::Structures.method(:number)  => [:byte,    :'java.lang.Byte',
                                             :short,   :'java.lang.Short',
                                             :int,     :'java.lang.Integer',
                                             :long,    :'java.lang.Long',
                                             :float,   :'java.lang.Float',
                                             :double,  :'java.lang.Double',
                                             :'java.lang.Number'],
      Javon::Structures.method(:character) => [:char, :'java.lang.Character'],
      Javon::Structures.method(:string)    => [:'java.lang.String',
                                               :'java.lang.CharSequence'],
      Javon::Structures.method(:class_object) => [:'java.lang.Class']}.each {
      |method, java_types|
      java_types.each {|type|
        @@structures_map[type] = method}}

    @@array_procedures = {
      :'java.lang.Class' => proc {|value|
        (value.is_a?(String) ? javon_from_ruby(value, :'java.lang.Class') :
         value)
        }
    }

    @@java_array_symbol = '[]'
    @@java_varargs_symbol = '...'
    def self.javon_from_ruby(value, type)
      # When dealing with Symbols and Strings
      # - [](String) works on both
      # - include?(String) works only on Strings
      if type[@@java_array_symbol]
        return convert_ruby_array(value, type.to_s.delete(@@java_array_symbol))
      end

      value_type = Javon::JavaHelpers.full_class_name(type)
      method = @@structures_map[value_type]

      return value if method.nil?

      # That's ugly. Named parameters seem the way to go but Ruby 2.1 isn't
      # THAT common...
      case method.arity.abs
      when 0 then method.call()
      when 1 then method.call(value)
      when 2 then method.call(value, value_type)
      end
    end

    class JavaException < RuntimeError
    end

    def self.convert_javon_array(array)
      array.map {|value|
        if value.is_a? Array then next(convert_javon_array(value)) end
        javon_to_ruby(value)
      }
    end
    
    def self.javon_to_ruby(structure)

      return structure if (!structure.is_a?(Hash) ||
                           structure['category'].nil?)

      category = structure['category'].to_sym
      type     = (structure['type'].to_sym if structure['type'])
      value    = structure['value']

      case category
      when :Boolean then return (value != false && value != 'false')
      when :Number

        case value
        when Numeric
          if (type == :char || type == :'java.lang.Character')
            return value.chr('UTF-8')
          else return value
          end
        when 'Infinity' then return Float::INFINITY
        when '-Infinity' then return -Float::INFINITY
        when 'NaN' then return Float::NAN
        else
          if ((String === value) && value.length == 1)
            return value[0]
          else raise 'Unknown numeric type : #{value} (#{value.class})'
          end
        end # case value

      when :Array
        #TODO
        convert_javon_array(value)
      when :Object

        case type
        when :String then return value
        when :Class then return value.to_sym
        when :Character

          case value
          when String then return value[0]
          when Numeric then return value.chr('UTF-8')
          else raise 'Unknown character type : #{value} (#{value.class})'
          end # case value

        end # case type

      when :Exception
        message = <<"EXCEPTION"
#{structure['message']}
Java Stacktrace ------------
#{structure['stacktrace']}
End of Java Stacktrace -----
EXCEPTION
        
        raise ::Javon::RubyAdapter::JavaException, message
      else
        structure
      end # case category

    end

    def self.convert_ruby_array(array, type)
      array_component_type = Javon::JavaHelpers.full_class_name(type)
      special_values_processing = @@array_procedures[array_component_type]

      Javon::Structures.array_object(array, array_component_type,
                                     &special_values_processing)

    end


    def self.invoke_method(receiver, name, *arguments, signature: nil)

      # The right name would be 'message receiver', but such name might
      # lead to confusion.
      if receiver.kind_of?(String) || receiver.kind_of?(Symbol)
        method_receiver = Javon::JavaHelpers.full_class_name(receiver)
      else
        method_receiver = receiver
      end
      method_name     = name.to_sym

      if !signature.nil?
        unless signature.kind_of? Enumerable
          parsed_signature =
            [Javon::JavaHelpers.full_class_name(signature)]
        else
          parsed_signature = signature.map {|classname|
            Javon::JavaHelpers.full_class_name(classname)
          }
        end

        converted_arguments = [].tap {|converted_args|
          (0..parsed_signature.length-1).map {|index|
            # In Java, varargs parameters (String...) are equivalent to splat
            # operators in Ruby (*arguments). They are placed at the end of
            # method signatures, capturing all the remaining arguments in an
            # Array object, and cannot be placed twice.
            # Methods using a varargs parameter are replaced by a compiler
            # generated method using an Array parameter instead, during the
            # compilation.
            # So 'public void method(java.lang.String... args)' becomes
            # 'public void method(java.lang.String[] args)' after
            # compilation.
            #
            # When using reflection, only the latter signature can be used
            # to retrieve the method to call.
            #
            # That said, this syntactic sugar can be imitated in Ruby so
            # there it is.

            current_signature = parsed_signature[index].to_s
            if current_signature.include?(@@java_varargs_symbol)
              # :'class...' -> :'class[]'
              parsed_signature[index] =
                current_signature.gsub(@@java_varargs_symbol,
                                       @@java_array_symbol).to_sym
              # [:a, :class[], :b, :c] -> [:a, :class[]]
              parsed_signature.replace(parsed_signature[0..index])
              converted_args[index] = javon_from_ruby(arguments[index..-1],
                                                      parsed_signature.last)
              break
            end

            if arguments[index].respond_to?(:javon_structure!)
              converted_args[index] = arguments[index].javon_structure!
            else
              converted_args[index] = javon_from_ruby(arguments[index],
                                                      parsed_signature[index])
            end
          }
        }

        if parsed_signature.length != converted_arguments.length
          raise(ArgumentError,
                "wrong number of arguments on the Java method (#{parsed_signature.length} for #{arguments.length})")
        end

      end # <= if !signature.nil?

      # And remember ! Local variables are set to nil, if they are declared in
      # non-executed code !
      if (method_name != :new)
        return Javon::Structures.invoke_method(method_receiver, method_name,
                                               parsed_signature,
                                               converted_arguments)
      else
        return Javon::Structures.new_object(method_receiver, parsed_signature,
                                            converted_arguments)
      end

    end

  end
end
