# -*- coding: utf-8 -*-
require 'minitest/unit'
require 'javon/ruby_adapter'
require 'json'

MiniTest::Unit.autorun

class TestRubyAdapter < MiniTest::Unit::TestCase

  def assert_javon_from_ruby(expected_structure, value, classname)
    assert_equal(expected_structure,
                 Javon::RubyAdapter.javon_from_ruby(value, classname))
  end

  def assert_number_structure(expected_class, expected_value,
                              provided_class, provided_value)
    assert_javon_from_ruby({category: :Number,
                           type: expected_class, value: expected_value},
                           provided_value, provided_class)
  end

  def test_number_conversions
    numeric_classes =
      [:byte, :short, :int, :long, :float, :double,
       :'java.lang.Byte',
       :'java.lang.Short',
       :'java.lang.Integer',
       :'java.lang.Long',
       :'java.lang.Float',
       :'java.lang.Double',
       :'java.lang.Number']
    short_numeric_classes =
     [:Byte, :Short, :Integer, :Long, :Float, :Double, :Number]
    values =
      [-789456, 13156, 0, 213.456798, 1e12]
    special_values =
      [[Float::INFINITY, "Infinity"],
       [-Float::INFINITY, "-Infinity"],
       [Float::NAN, "NaN"]]

    numeric_classes.each {|numeric_class|
      values.each {|value|
        assert_number_structure(numeric_class, value, numeric_class, value)
      }
      special_values.each {|pair|
        assert_number_structure(numeric_class, pair[1], numeric_class, pair[0])
      }
    }
    short_numeric_classes.each {|numeric_class|
      values.each {|value|
        assert_number_structure("java.lang.#{numeric_class}".to_sym, value,
                                numeric_class, value)
      }
      special_values.each {|pair|
        assert_number_structure("java.lang.#{numeric_class}".to_sym, pair[1],
                                numeric_class, pair[0])
      }
    }
  end

  def test_character_conversions
    character_classes = [:char, :'java.lang.Character', :'Character']
    characters = ["a", "e", "z"]
    codepoints = [12, 78, 456]
    character_classes.each {|character_class|
      characters.each {|character|
        assert_javon_from_ruby({category: :Object, type: :Character,
                                 value: character},
                               character, character_class)
      }
      codepoints.each {|codepoint|
        assert_number_structure(:char, codepoint, :char, codepoint)
      }
    }
  end

  def test_string_conversions
    string_classes = [:'java.lang.String',       :String,
                      :'java.lang.CharSequence', :CharSequence]
    strings = ["a", "Hello", "今日は"]
    strings.each {|string|
      string_classes.each {|string_class|
        assert_javon_from_ruby({category: :Object, type: :String,
                                 value: string},
                               string, string_class)
      }
    }

  end

  def test_boolean_conversions
    boolean_classes = [:boolean, :'java.lang.Boolean', :Boolean]
    true_values  = [true, "a", 0]
    false_values = [false, nil]
    boolean_test = proc {|expected_value, value, provided_class|
      assert_javon_from_ruby({category: :Boolean, value: expected_value},
                             value, provided_class)
    }
    boolean_classes.each {|boolean_class|
      true_values.each {|value|
        boolean_test.call(true, value, boolean_class)
      }
      false_values.each {|value|
        if (boolean_class != :boolean && value.nil?)
          assert_javon_from_ruby({category: :Object, type: :null},
                                 value, boolean_class)
        else
          boolean_test.call(false, value, boolean_class)
        end
      }
    }
  end

  def test_class_objects_conversions
    object_classnames = [:int, 'java.lang.Number', 'java.lang.Exception']
    java_class_names = [:'java.lang.Class', :Class]
    object_classnames.each {|object_classname|
      java_class_names.each {|java_class_name|

        assert_javon_from_ruby({category: :Object, type: :Class,
                                value: object_classname.to_s},
                                object_classname, java_class_name)
      }
    }
  end

  def test_null_conversions

    accept_null_values = [:Boolean, :Byte, :Short, :Character,
                          :Integer, :Long, :Float, :Double,
                          :Number, :String, :CharSequence,
                          :Class]
    accept_null_values.each {|classname|
      assert_javon_from_ruby({category: :Object, type: :null},
                             nil, classname)
    }

  end

  def test_array_conversions

    infinity = proc {|type, sign|
      Javon::Structures.number((sign == :- ? -Float::INFINITY :
                                              Float::INFINITY), type) }
    nan = proc {|type|
      Javon::Structures.number(Float::NAN, type) }

    array_type_literals = ['int[]', 'long[]']
    short_array_type_literals = ['String[]', 'Exception[]']
    simple_array = [1, 2, 'a', 78]
    simple_nested_array = [[1, 2, 'a'], ['z', 9]]

    array_type_literals.each {|literal|
      component_type = literal.gsub('[]', '').to_sym
      assert_javon_from_ruby({category: :Array,
                              type: component_type,
                              value: simple_array},
                              simple_array, literal)

      assert_javon_from_ruby({category: :Array,
                              type: component_type,
                              value: simple_nested_array},
                              simple_nested_array, literal)

      assert_javon_from_ruby({category: :Array,
                              type: component_type,
                              value: [infinity[component_type],
                                      infinity[component_type],
                                      infinity[component_type, :-],
                                      nan[component_type],
                                      'a' ]},
                              [Float::INFINITY, 2*10**400,
                               -4**800, Float::NAN, 'a'], literal)

      assert_javon_from_ruby({category: :Array,
                             type: component_type,
                             value: [infinity.call(component_type),
                                    ['a', 32, nan.call(component_type)],
                                    [infinity.call(component_type, :-)]]},
                             [Float::INFINITY, ['a', 32, Float::NAN],
                                               [-785**200]], literal)
    }

    short_array_type_literals.each {|literal|
      component_type = "java.lang.#{literal.gsub('[]', '')}".to_sym
      assert_javon_from_ruby({category: :Array,
                              type: component_type,
                              value: simple_array},
                              simple_array, literal)

      assert_javon_from_ruby({category: :Array,
                              type: component_type,
                              value: [infinity[:'java.lang.Number'],
                                      infinity[:'java.lang.Number'],
                                      infinity[:'java.lang.Number', :-],
                                      nan[:'java.lang.Number'] ]},
                              [Float::INFINITY, 2*10**400, -4**800, Float::NAN],
                              literal)
    }

    expected_class_array = simple_array.clone
    expected_class_array[2] = Javon::Structures.class_object('a')

    expected_nested_class_array = simple_nested_array.clone
    expected_nested_class_array[0][2] = Javon::Structures.class_object('a')
    expected_nested_class_array[1][0] = Javon::Structures.class_object('z')

    assert_javon_from_ruby({category: :Array, type: :'java.lang.Class',
                            value: expected_class_array},
                           simple_array, 'Class[]')

    assert_javon_from_ruby({category: :Array, type: :'java.lang.Class',
                           value: expected_nested_class_array},
                           simple_nested_array, 'Class[][]')


  end

  def test_method_call

    test_object = Javon::Structures.reference(1)

    assert_equal(Javon::Structures.invoke_method(test_object, :nya),
                 Javon::RubyAdapter.invoke_method(test_object, "nya"))
    assert_equal(Javon::Structures.invoke_method(test_object, :equal,
                                                 [:int],
                                                 [Javon::Structures.number(27,:int)]),
                 Javon::RubyAdapter.invoke_method(test_object, "equal",
                                                  27, signature: :int))
    assert_equal(Javon::Structures.invoke_method(test_object, :equal,
                                                 [:int],
                                                 [Javon::Structures.number(27,:int)]),
                 Javon::RubyAdapter.invoke_method(test_object, "equal",
                                                  27, signature: 'int'))
    assert_equal(Javon::Structures.invoke_method(test_object, :concat,
                                                 [:'java.lang.String[]'],
                                                 [Javon::Structures.array_object(['a', 'b', '7'],
                                                                                 :'java.lang.String')]),
                 Javon::RubyAdapter.invoke_method(test_object, "concat",
                                                  'a', 'b', '7',
                                                  signature: 'String...'))
    assert_equal(Javon::Structures.invoke_method(test_object, :concat,
                                                 [:'java.lang.String',
                                                  :'java.lang.String'],
                                                 [Javon::Structures.string('it is'),
                                                  Javon::Structures.string('true')]),
                 Javon::RubyAdapter.invoke_method(test_object, 'concat',
                                                  'it is', 'true',
                                                  signature: [:String, :String]))


    assert_equal(Javon::Structures.new_object('java.lang.String'),
                 Javon::RubyAdapter.invoke_method('java.lang.String', 'new'))
    
  end


  def call_javon_to_ruby(javon_structure)
    structure = JSON.parse(javon_structure.to_json)
    Javon::RubyAdapter.javon_to_ruby(structure)
  end
  def assert_javon_to_ruby(expected_value, javon_structure)
    assert_equal(expected_value, call_javon_to_ruby(javon_structure))
  end

  def test_javon_to_ruby
    assert_javon_to_ruby(true,  Javon::Structures.boolean(true))
    assert_javon_to_ruby(false, Javon::Structures.boolean(false))
    assert_javon_to_ruby("a", Javon::Structures.character('a'))
    assert_javon_to_ruby("a", Javon::Structures.character(97))
    assert_javon_to_ruby(120, Javon::Structures.number(120, :byte))
    assert_javon_to_ruby(-8748, Javon::Structures.number(-8748, :short))
    assert_javon_to_ruby('カ', Javon::Structures.number(12459, :char))
    assert_javon_to_ruby('ᵮ', Javon::Structures.number('ᵮ', :char))
    assert_javon_to_ruby('z', Javon::Structures.number('z', :int))
    assert_javon_to_ruby(7894552456,
                         Javon::Structures.number(7894552456, :long))
    assert_javon_to_ruby(467.156, Javon::Structures.number(467.156, :float))
    assert_javon_to_ruby(-7894569.15,
                         Javon::Structures.number(-7894569.15, :double))
    assert_javon_to_ruby(Float::INFINITY,
                         Javon::Structures.number(Float::INFINITY, :float))
    assert_javon_to_ruby(-Float::INFINITY,
                         Javon::Structures.number(-Float::INFINITY, :double))
    assert(call_javon_to_ruby(Javon::Structures.number(Float::NAN, :double)).nan?)
    assert_javon_to_ruby(-78,
                         Javon::Structures.number(-78, :'java.lang.Byte'))
    assert_javon_to_ruby('e',
                         Javon::Structures.number('e', :'java.lang.Character'))
    assert_javon_to_ruby("hello",
                         Javon::Structures.string("hello"))
    assert_javon_to_ruby(:'java.lang.CharSequence',
                         Javon::Structures.class_object('java.lang.CharSequence'))
    assert_javon_to_ruby([1, 2, 'a', [Float::INFINITY]],
                         Javon::Structures.array_object([1, 2, 'a', 
                                                        [Float::INFINITY]],
                                                        :int))
  end
end
