# -*- coding: utf-8 -*-
require 'minitest/unit'
require 'javon/java_helpers'

MiniTest::Unit.autorun

class TestRubyAdapter < MiniTest::Unit::TestCase
  def test_is_primitive
    [:void, :boolean, :byte, :short, :char,
     :int, :long, :float, :double].each {|primitive_typename|
      assert(Javon::JavaHelpers.is_primitive(primitive_typename))
    }
    [:'java.lang.String', :'java.lang.Integer', :'java.lang.Character',
     nil].each {|non_primitive|
      assert(!Javon::JavaHelpers.is_primitive(non_primitive))
    }
  end

  def test_full_class_name
    [[:String,            :'java.lang.String'],
     [:'Exception[]',     :'java.lang.Exception[]'],
     [:'Number[][][]',    :'java.lang.Number[][][]'],
     [:'CharSequence...', :'java.lang.CharSequence...']].each {|classnames|
      assert_equal(classnames[1],
                   Javon::JavaHelpers.full_class_name(classnames[0]))
    }
  end

  def test_is_numeric_type
    [:short, :char, :int, :long, :float, :double,
     :'java.lang.Byte', :'java.lang.Short', :'java.lang.Character',
     :'java.lang.Integer', :'java.lang.Long', :'java.lang.Float',
     :'java.lang.Double', :'java.lang.Number'].each {|numeric_class_name|
      assert(Javon::JavaHelpers.is_numeric_type?(numeric_class_name))
    }

    [:'java.math.BigInteger', :'java.math.BigDecimal'].each {|other_class_name|
      assert(!Javon::JavaHelpers.is_numeric_type?(other_class_name))
    }
  end
end
