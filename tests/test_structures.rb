# -*- coding: utf-8 -*-
require 'minitest/unit'
require 'javon/structures'

MiniTest::Unit.autorun

class TestJavonStructures < MiniTest::Unit::TestCase
  def test_number
    int_number = Javon::Structures.number(145, :int)
    assert_equal({category: :Number, type: :int, value: 145}, int_number)

    byte_number = Javon::Structures.number(789, :byte)
    assert_equal({category: :Number, type: :byte, value: 789}, byte_number)

    double_number = Javon::Structures.number(1232.12, :double)
    assert_equal({category: :Number, type: :double, value: 1232.12},
                 double_number)

    infinite_number = Javon::Structures.number(1e500, :float)
    assert_equal({category: :Number, type: :float, value: "Infinity"},
                 infinite_number)

    negative_infinity = Javon::Structures.number(-2e450, :long)
    assert_equal({category: :Number, type: :long, value: "-Infinity"},
                 negative_infinity)

    not_a_number = Javon::Structures.number(Float::NAN, :char)
    assert_equal({category: :Number, type: :char, value: "NaN"},
                 not_a_number)

    too_big_number = Javon::Structures.number(("1"+"0"*500).to_i, :int)
    assert_equal({category: :Number, type: :int, value: "Infinity"},
                 too_big_number)

    character_as_number = Javon::Structures.number("z", :char)
    assert_equal({category: :Number, type: :char, value: "z"},
                 character_as_number)
                 
    wrong_type_integer = Javon::Structures.number(75, :doesnotexist)
    assert_equal({category: :Number, type: :'java.lang.Number', value: 75},
                 wrong_type_integer)
    
    wrong_type_float = Javon::Structures.number(75.1, :doesnotexist)
    assert_equal({category: :Number, type: :'java.lang.Number', value: 75.1},
                 wrong_type_float)
  end

  def test_string
    string_value = Javon::Structures.string("a")
    assert_equal({category: :Object, type: :String, value: "a"}, string_value)
  end

  def test_class
    class_value = Javon::Structures.class_object("java.lang.String")
    class_value_symbol = Javon::Structures.class_object(:'java.lang.String')
    assert_equal({category: :Object, type: :Class, value: "java.lang.String"},
                 class_value)
    assert_equal({category: :Object, type: :Class, value: "java.lang.String"},
                 class_value_symbol)
  end

  def test_null
    null_value = Javon::Structures.null()
    assert_equal({category: :Object, type: :null}, null_value)
  end

  def test_character
    character_value = Javon::Structures.character("a")
    assert_equal({category: :Object, type: :Character, value: "a"},
                 character_value)

    codepoint_value = Javon::Structures.character(1234)
    assert_equal({category: :Number, type: :char, value: 1234},
                 codepoint_value)
  end

  def test_boolean
    true_value = Javon::Structures.boolean(true)
    assert_equal({category: :Boolean, value: true},
                 true_value)
    false_value = Javon::Structures.boolean(false)
    assert_equal({category: :Boolean, value: false},
                 false_value)
  end

  def test_reference
    reference_id = Javon::Structures.reference(12)
    assert_equal({category: :Object, type: :Reference, value: 12},
                 reference_id)
    reference_id_string = Javon::Structures.reference("a")
    assert_equal({category: :Object, type: :Reference, value: "a"},
                 reference_id_string)
  end
  
  def test_array_object
    test_array = [1, 2, "a", Object.new]
    array_structure = Javon::Structures.array_object(test_array, :int)  
    assert_equal({category: :Array, type: :int, value: test_array},
                 array_structure)
    
    modified_array_structure = Javon::Structures.array_object(test_array,
                                                              :int) {|value|
                                 (value.kind_of?(Numeric) ? value * 2 : value)}
    expected_modified_array = [2, 4, test_array[2], test_array[3]]
    assert_equal({category: :Array, type: :int, value: expected_modified_array},
                 modified_array_structure)
                 
    bad_numbers_array = [Float::INFINITY, -Float::INFINITY, Float::NAN,
                         24*10**400]
    bad_numbers_structure = Javon::Structures.array_object(bad_numbers_array, 
                                                           :wtfisthis)
    expected_structure_from_bad_numbers = {category: :Array, type: :wtfisthis,
      value: [Javon::Structures.number(Float::INFINITY, :'java.lang.Number'),
              Javon::Structures.number(-Float::INFINITY, :'java.lang.Number'),
              Javon::Structures.number(Float::NAN, :'java.lang.Number'),
              Javon::Structures.number(Float::INFINITY, :'java.lang.Number')]}
    assert_equal(expected_structure_from_bad_numbers,
                 bad_numbers_structure)
  end
                 
end
