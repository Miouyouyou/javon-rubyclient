require 'minitest/unit'
require 'javon/client'

MiniTest::Unit.autorun

class TestJavonClient < MiniTest::Unit::TestCase
  include JavonClient
  
  def test_new_and_methods
    list = java_new "java.util.ArrayList"
    list.add("Miou")
    assert_equal "Miou", list.get(0)
  end
  
  def test_invoke_method_on_simple_objects
    a = java_new 'java.lang.String', "A"
    assert_equal 1, a.length
    assert_equal 1, a.javon.length()
    assert_equal Comparator.class, a.javon.CASE_INSENSITIVE_ORDER.class
    
    b = java_new 'java.lang.String', ["h", "i"], {signature: :'char[]'}
    assert_equal 2, b.length
    assert_equal 2, b.javon.length()
  end
  
  def test_invoke_static_methods
    assert_false(static('java.lang.Character.isHighSurrogate', 
                        "A", 
                        {signature: [:char]}))
  end
end
